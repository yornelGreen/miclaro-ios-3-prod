$(function() {

	// Update App View
	// ---------------
	
	app.views.UpdateAppView = app.views.CommonView.extend({

		name:'update_app',
		
		// The DOM events specific.
		events: {
			
			// content
			'click #update-app'         :'updateApp',

            // footer
            'click #btn-help'           :'helpSection'
				
		},

		// Render the template elements        
		render: function(callback) {
			var self = this,
				variables = {};
			
			app.TemplateManager.get(self.name, function(code){
		    	var template = cTemplate(code.html());
		    	$(self.el).html(template(variables));	
		    	
		    	callback();	
		    	return this;
		    });					
		
		},
		
		updateApp: function(){
			window.open('market://details?id='+app.packageName, '_system');
		}
	});
});
